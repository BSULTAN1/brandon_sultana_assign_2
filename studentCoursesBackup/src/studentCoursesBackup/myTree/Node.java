package studentCoursesBackup.driver;
import java.util.ArrayList;

public class Node implements SubjectI, ObserverI, Cloneable{
    
    //B-Number of the Node. Used for determining the location of an element in the tree
    private int bNum;
    
    //A List of all courses taken by a given student
    private ArrayList<String> takenCourses = new ArrayList<String>();
    
    //Left and Right Children of a Node. Used for tree traversal
    private Node leftChild;
    private Node rightChild;
    
    //A list of two observer Nodes to a Node. These Nodes have the same content as the main Node
    private ArrayList<Node> observers = new ArrayList<Node>();

    /** Empty constructor for Node, Used for creating a Failure Node
    * @param (none)
    * @return (void)
    */
    public Node() {
        bNum = -1;
    }
    
    /** Value constructor for Node, takes in a B-Number and a Course
    * @param (int) bNumber - The B-Number of the node to be created
    * @param (String) course - The first course for the new Node
    * @return (void)
    */
    public Node(int bNumber, String course) {
        bNum = bNumber;
        addCourse(course);
    }
  
    /** Accessor for B-Number
    * @param (void)
    * @return (int) bNum - The Nodes B-Number
    */
    public int getBNumber() {
        return bNum;
    }

    /** Accessor for Left Child
    * @param (void)
    * @return (int) leftChild - The Left Child Node of the current Node
    */
    public Node getLeft() {
        return leftChild;
    }
    
    /** Mutator for Left Child
    * @param (Node) newLeft - The new Left Child Node for the current Node
    * @return (void)
    */
    public void setLeft(Node newLeft) {
        leftChild = newLeft;
    }

    /** Accessor for Right Child
    * @param (void)
    * @return (int) rightChild - The Right Child Node of the current Node
    */
    public Node getRight() {
        return rightChild;
    }

    /** Mutator for Right Child
    * @param (Node) newRight - The new Right Child Node for the current Node
    * @return (void)
    */
    public void setRight(Node newRight) {
        rightChild = newRight;
    }
    
    /** Accessor for observers
    * @param (void)
    * @return (ArrayList<Node>) observers - The list of observer nodes for the current node
    */
    public ArrayList<Node> getObservers() {
        return observers;
    }
    
    /** Adds an observer to the observer list
    * @param (void)
    * @return (Node) n - The Node to be added
    */
    public void addObserver(Node n) {
        observers.add(n);
    }
    
    /** Adds a course to the courseList
    * @param (String) course - The course to be added to the course list
    * @return (void)
    */
    public void addCourse(String course) {
        takenCourses.add(course);
    }
    
    /** Accessor for the courseList
    * @param (void)
    * @return (ArrayList<String>) - The List of courses to be returned
    */
    public ArrayList<String> getCourses() {
        return takenCourses;
    }
    
    /** Deletes a Course from the Course List
    * Set an initital delete index at -1
    * Loop through the arrayList to find if the element to be deleted is found
    * If it is, save the index and delete the element at that index
    * If it is not, do nothing
    * @param (String) course- The course to be deleted from the list
    * @return (void)
    */
    public void deleteCourse(String course) {
        int index = -1;
        
        for(int i = 0; i < getCourses().size(); i++) {
            if(getCourses().get(i).equals(course)) {
                index = i;
            }
        }
        if(index != -1) {
            getCourses().remove(index);
        }
    }
    
    /** Creates a copy of the current Node
    * Creates a new node using the same bNumber and course as the current Node
    * Then return the new Node
    * @param (void)
    * @return (Node) clone - A copy of the current Node
    */
    public Node clone() {
        Node clone = new Node(bNum, getCourses().get(0));
        return clone;
    }
    
    /** Converts the Node to a String representation
    * First determines the size of the B-Number of the Node
    * Depending on the size, convert it to a string and add 0's to the front to give it 4 digits
    * Then append a ":" to the B-Number, and loop through the arrayList, adding the courses to the string
    * Return the completed String
    * @param (void)
    * @return (String) ret - The String representation of the Node
    */
    public String toString() {
        String str = "";
        if(bNum < 10) {
            str = Integer.toString(bNum);
            str = "000" + str;
        }
        else if(bNum < 100) {
            str = Integer.toString(bNum);
            str = "00" + str;
        }
        else if(bNum < 1000) {
            str = Integer.toString(bNum);
            str = "0" + str;
        }
        else {
            str = Integer.toString(bNum);
        }
        
        String ret = str + ":";
        for(int i = 0; i < takenCourses.size(); i++) {
            if(i != takenCourses.size()-1) {
               ret = ret + takenCourses.get(i) + ", "; 
            }
            else {
                ret = ret + takenCourses.get(i);
            }
            
        }
        return ret;
    }
    
    /** Notifies observers of a change
    * Loop through each observer in the observer list
    * Call update on each observer to update their course list
    * @param (String) course - The course to be removed from the Observers course list
    * @return (void)
    */
    public void notifyAllObservers(String course) {
        for(int i = 0; i < observers.size(); i++) {
            observers.get(i).update(course);
        }
        
    }

    /** Observer calls delete on the course to remove it
    * @param (String) course - The course to be removed from the Observers course list
    * @return (void)
    */
    public void update(String course) {
        deleteCourse(course);
    }
    
}