package studentCoursesBackup.driver;
import java.util.ArrayList;

public interface ObserverI {
    public void update(String course);
}