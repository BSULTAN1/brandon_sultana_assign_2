package studentCoursesBackup.driver;
import java.util.ArrayList;

public class Driver {
    
    /** Checks Command Line arguments
    * Reads input from the first two files and creates three trees based off of them
    * Then calls functions to output these trees to three separate output files
    * @param (String[]) args - The command line arguments of the program
    * @return (Node) n - The Node to be added
    */
    public static void main(String[] args) {
        //Check if the number of arguments is sufficient
        if(args.length != 5) {
            System.err.println("Usage <inputFile> <deleteFile> <outputFile1> <outputFile2> <outputFile3>");
            System.exit(1);
            
        }
        if(args[0].equals("${arg0}") || args[1].equals("${arg1}") || args[2].equals("${arg2}") || args[3].equals("${arg3}") || args[4].equals("${arg4}")) {
            System.err.println("Usage <inputFile> <deleteFile> <outputFile1> <outputFile2> <outputFile3>");
            System.exit(1);
        }
        
        //Read the input file into an ArrayList of Strings
        FileProcessor fp1 = new FileProcessor();
        fp1.readFile(args[0]);
        ArrayList<String> toAdd = fp1.getLines();
        
        //Read the first element of the ArrayList and create the root node from it and the tree
        String[] dataArray = toAdd.get(0).split(":");
        Node root = new Node(Integer.parseInt(dataArray[0]), dataArray[1]); 
        
        //Clone the root node twice, add these clones as observers to the original
        Node clone1 = root.clone();
        Node clone2 = root.clone();
        root.addObserver(clone1);
        root.addObserver(clone2);
        
        //Create three trees using the three roots
        TreeBuilder mainTree = new TreeBuilder(root);
        TreeBuilder backup1 = new TreeBuilder(clone1);
        TreeBuilder backup2 = new TreeBuilder(clone2);
        
        //Loop through the rest of the arraylist filling out the trees
        for(int i = 1; i < toAdd.size(); i++) {
            
            //Split the string by the colon
            dataArray = toAdd.get(i).split(":");
            
            //Create a node based on the B-Number
            Node node = new Node(Integer.parseInt(dataArray[0]), dataArray[1]);
            
            //Clone this new node twice
            Node node_clone1 = node.clone();
            Node node_clone2 = node.clone();
            
            //Add the clones as observers to the original node
            node.addObserver(node_clone1);
            node.addObserver(node_clone2);
            
            //Insert the original node and the two clones into their respective trees
            mainTree.insert(node);
            backup1.insert(node_clone1);
            backup2.insert(node_clone2);
        }

        //Read the delete file into an ArrayList of Strings
        FileProcessor fp2 = new FileProcessor();
        fp2.readFile(args[1]);
        ArrayList<String> toDelete = fp2.getLines();
        
        //Loop through the arraylist and delete the courses from the nodes
        for(int i = 0; i < toDelete.size(); i++) {
            dataArray = toDelete.get(i).split(":");
            mainTree.delete(dataArray);
        }
        
        //Create instances of Results and set the File name
        Results results1 = new Results(args[2]);
        Results results2 = new Results(args[3]);
        Results results3 = new Results(args[4]);
        
        //Sort the nodes into an ArrayList
        mainTree.sortNodes(results1, root);
        backup1.sortNodes(results2, clone1);
        backup2.sortNodes(results3, clone2);
        
        //Write the Trees to files
        results1.writeToFile(); 
        results2.writeToFile();
        results3.writeToFile();
    }
}