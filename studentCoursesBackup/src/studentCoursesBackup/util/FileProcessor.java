package studentCoursesBackup.driver;

import java.util.ArrayList;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class FileProcessor {
    //Use ArrayList to hold all the strings from the line
    private ArrayList<String> lines = new ArrayList<String>();
    
    //Return the size of the ArrayList to be used for the for loop
    public int getNumLines() {
        return lines.size();
    }
    
    public ArrayList<String> getLines() {
        return lines;
    }
    
    /** Tries to read the given file, falls into an exception upon failure.
    * If successful, each line is added to an ArrayList
    * File is then closed
    * @param (String) fileName - The name of the file to be outputted to
    * @return (void)
    */
    public void readFile(String fileName) {
        //Try to read the input file
        File inFile = new File(fileName);
        BufferedReader read = null;
        
        //Attempt to read the file
        try {
            read = new BufferedReader(new FileReader(inFile));
            String line;
            while((line = read.readLine()) != null) {
                lines.add(line);
            }
        }
        //Catch File Not Found
        catch(FileNotFoundException e) {
            System.err.println("ERROR: Unable to open input file");
            System.exit(0);
        }
        //Catch IO Exception
        catch(IOException e) {
            System.err.println("ERROR: IO Exception");
            System.exit(0);
        }
        finally {
            try{
                read.close();
            }
            catch(IOException e) {
                System.err.println("Error: IO Exception");
                System.exit(0);
            }
        }
    }
    
    /** Use an index to find a String from an ArrayList and return it
    * @param (int) index - the desired index from the ArrayList
    * @return (String) - returns a line from the file from the ArrayList
    */
    public String readLine(int index) {
        return lines.get(index);
    }
    
    /** Dummy toString implementation
    * @param none
    * @return (String) - returns an empty string
    */
    public String toString() {
        return "";
    }
}