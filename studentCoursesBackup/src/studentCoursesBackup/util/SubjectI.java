package studentCoursesBackup.driver;
import java.util.ArrayList;

public interface SubjectI {

    public void notifyAllObservers(String course);
}