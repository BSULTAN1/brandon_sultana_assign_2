package studentCoursesBackup.driver;
import java.util.ArrayList;
import java.io.File;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.FileNotFoundException;
import java.io.IOException;


public class Results implements FileDisplayInterface, StdoutDisplayInterface {
    private ArrayList<String> nodeList = new ArrayList<String>();
    private String file;

    /** Mutator for File Name
    * @param (String) fileName - The name of the file from which to read data
    * @return (void)
    */
    public void setFile1(String fileName) {
        file = fileName;
    }
    
    /** Value constructor
    * @param (String) fileName - The name of the file from which to read data
    * @return (void)
    */
    public Results(String fileName) {
        file = fileName;
    }
    
    /** Takes the string representation of a node and adds it to an ArrayList for file output later
    * @param (Node) n - The node to be added to the Array List
    * @return (void)
    */
    public void addToList(Node n) {
        nodeList.add(n.toString());
    }

    /** Outputs the nodeList to a file
    * 
    * @param (void)
    * @return (void)
    */
    public void writeToFile() {
        try {
            FileWriter fw = new FileWriter(file);
            BufferedWriter bw = new BufferedWriter(fw);
            
            for(int i = 0; i < nodeList.size(); i++) {
                bw.write(nodeList.get(i));
                bw.write("\n");
            }
            
            bw.close();
            
        }
        catch(FileNotFoundException e) {
            System.err.println("ERROR: Unable to open output file");
            System.exit(0);
        }
        //Catch IO Exception
        catch(IOException e) {
            System.err.println("ERROR: IO Exception");
            System.exit(0);
        }
    }
    
    public void writeToStdout() {
        for(int i = 0; i < nodeList.size(); i++) {
            System.out.println(nodeList.get(i));
        }
    }
    
}