package studentCoursesBackup.driver;

public class TreeBuilder {
    private Node root;
    private Node failure = new Node();
    
    /** Value Constructor for TreeBuilder, creates a tree with just a root node
    * @param (Node) first - The root of the tree
    * @return (void)
    */
    public TreeBuilder(Node first) {
        root = first;
    }
    
    /** Traverses the tree to find a given Node based off of its B-Number
    * Starts at the root node
    * Falls into a while loop and traverses left or right depending on the value to be found
    * If the value is found the Node is returned
    * Otherwise, a failure Node with a B-Number of -1 is returned
    * @param (int) number - The B-Number of the desired node
    * @return (Node) the node that was supposed to be found
    */
    public Node search(int number) {
        Node current = root;
        int found = -1;
        while(current != null && found == -1) {
            if(number == current.getBNumber()) {
                found = 0;
            }
            else if(number <= current.getBNumber()) {
                current = current.getLeft();
            }
            else {
                current = current.getRight();
            }
            
        }
        if(found != -1) {
            return current;
        }

        return failure; 
    }
    
    /** Inserts a Node into the tree:
    * Starts at the root node
    * Falls into a while loop and traverses left or right depending on the value to be inserted
    * If the value already exists, the loop ends, and the course is added to that node if it is not already present
    * Otherwise, it keeps traversing until it finds an empty Node, where it will then insert the Node
    * @param (Node) node - The node to be inserted
    * @return none
    */
    public void insert(Node node) {
        Node current = root;
        int found = -1;
        while(current != null && found == -1) {
            //If the BNumber is found, exit the loop
            if(node.getBNumber() == current.getBNumber()) {
                found = 0;
            }
            else if(node.getBNumber() <= current.getBNumber()) {
                if(current.getLeft() == null) {
                    current.setLeft(node);
                    break;
                }
                else {
                    current = current.getLeft(); 
                }
                
            }
            else {
                if(current.getRight() == null) {
                    Node temp = current;
                    current.setRight(node);
                    break;
                }
                else {
                    current = current.getRight(); 
                }
            }
            
        }
        //Add the course from the node to the pre-existing node if the B-Number is already present and that course is not in the ArrayList
        if(found == 0) {
            int inArray = 0;
            for(int i = 0; i < current.getCourses().size(); i++) {
                if(current.getCourses().get(i).equals(node.getCourses().get(0))) {
                    inArray = -1;
                }
            }
            if(inArray == 0) {
                current.addCourse(node.getCourses().get(0));
            }
            
        }
    }
    
    /**Deeletes a Node from the tree:
    * Calls search to find a desired node.
    * Then deletes a course from that node.
    * @param (String[]) params - A list of two strings, the first specifying the B-Number of a Node, the second being the course to delete
    * @return none
    */
    public void delete(String[] params) {
        Node toDelete = search(Integer.parseInt(params[0]));
        if(toDelete.getBNumber() == -1) {
            return;
        }
        
        toDelete.deleteCourse(params[1]);
        toDelete.notifyAllObservers(params[1]);
    }
    
    //http://k2code.blogspot.com/2011/05/print-binary-search-tree-in-increasing.html
    /** Outputs the tree in ascending order to Results: 
    * Initially takes in the root of the tree
    * Recursively calls the function on the leftChild
    * Then outputs the currentNode to the Results class
    * Then recursively calls the function on the rightChild
    * @param (Node) current - The current node being worked on
    * @param (Results) r - The results class to which the Nodes are outputted.
    * @return none
    */
    public void sortNodes(Results r, Node current) {
        if(current == null) {
            return;
        }
        sortNodes(r, current.getLeft());
        r.addToList(current);
        sortNodes(r, current.getRight());
        
    }
    
}