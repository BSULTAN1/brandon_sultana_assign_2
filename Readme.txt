﻿
Assuming you are in the directory containing this README:

## To clean:
ant -buildfile src/build.xml clean

-----------------------------------------------------------------------
## To compile: 
ant -buildfile src/build.xml all

-----------------------------------------------------------------------
## To run by specifying arguments from command line 
## We will use this to run your code
ant -buildfile src/build.xml run -Darg0=FIRST -Darg1=SECOND -Darg2=THIRD -Darg3=FOURTH -Darg4=FIFTH

-----------------------------------------------------------------------

## To create tarball for submission
ant -buildfile src/build.xml tarzip or tar -zcvf firstName_secondName_assign_number.tar.gz firstName_secondName_assign_number

-----------------------------------------------------------------------

"I have done this assignment completely on my own. I have not copied
it, nor have I given my solution to anyone else. I understand that if
I am involved in plagiarism or cheating I will have to sign an
official form that I have cheated and that this form will be stored in
my official university record. I also understand that I will receive a
grade of 0 for the involved assignment for my first offense and that I
will receive a grade of F for the course for any additional
offense.”

Brandon Sultana
[Date: 09/29/2017]

-----------------------------------------------------------------------

Provide justification for Data Structures used in this assignment in
term of Big O complexity (time and/or space)

Use of an ArrayList for storing the lines of the input file
O(n) for time and space complexity. Items simply added to a contained array using O(1). Items copied over to a new array if space runs out using O(n).

Use Binary Search Tree for storing Nodes. Each node can have at most two children, where the left child has a B-Number less than the parent, and the right child has a B-Number greater than the parent. These Nodes also hold an ArrayList of courses, but this isn't used for tree traversal.

Search, Insert, and Delete operations on average operate at O(log(N)) complexity. For each step down the tree, several nodes are removed from potential traversal, unless all nodes are in a direct chain.
The best case for these traversals is O(1), for if the desired Node is the root of the tree.
The worst case for these traversals is O(N), because the worst kind of tree to traverse is one which acts as an ArrayList, where each Node is the child of the one before it.

The Best case for In-Order traversal which is used for printing all the nodes in the tree is O(n), where the tree is a chain of right children. This is because the tree will start at the root, and simply traverse right every time until it reaches the end
The worst case is O(n^2), for a chain of left children. The traversal must traverse all the way down to the left most leaf, and then backtrack all the way back up to the root
The average Case for In-Order traversal, which is used for printing all the nodes in the tree is O(nlog(n)), because each node must be visited at least once in order to output them all, but some must be visited multiple times while backtracking.


-----------------------------------------------------------------------

Provide list of citations (urls, etc.) from where you have taken code
(if any).

//http://k2code.blogspot.com/2011/05/print-binary-search-tree-in-increasing.html
Used for In-Order traversal of the tree

